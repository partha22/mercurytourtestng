package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginRegressionSuit {
	
	WebDriver driver;
	
	MethodRepository md =new MethodRepository();
	
	@BeforeMethod
	public void Applaunch() throws InterruptedException{
		
		md.browserApplicationLaunch();
	}
	
	/*
	 * TC_001 :Verify the valid login functionality
	 */
	@Test(priority = 0,enabled = true,description = "Verify the valid login functionality")
	public void Verifyvalidlogin()
	{
		try{
			
			md.login("dasd","dasd");
			Assert.assertEquals(md.verifyValidLogin(), true);
			
		}catch(Exception e){
			
			System.out.println(e);
			
		}
		
	}
	
	/*
	 * TC_002 : Verify Invalid login functionality
	 */
	@Test(priority = 1,enabled = false,description = "Verify the invalid login functionality")
	public void VerifyInvalidlogin()
	{
		md.login("dasd1","dasd1");
		Assert.assertEquals(md.verifyInvalidLogin(), true);
	}
	
	
	@AfterMethod
	public void apClose(){
		
		md.Appclose();
	}

}
