package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FlightFinderRegressionSuit {
	
	WebDriver driver;
	
	MethodRepository md =new MethodRepository();
	
	@BeforeMethod
	public void Applaunch() throws InterruptedException{
		
		md.browserApplicationLaunch();
	}
	
	
	/*
	 * TC_001 :Verify default selection of flight type
	 */
	@Test(priority = 0,enabled = true,description = "TC_001 :Verify default selection of flight type")
	public void VerifyDefaultFlightType()
	{
		try{
			
			md.login("dasd","dasd");
			Assert.assertEquals(md.VerifyDefaultRoundTrip(), true);
			
		}catch(Exception e){
			
			System.out.println(e);
			
		}
		
	}
	
	/*
	 * TC_002 : Verify departing from value selection
	 */
	@Test(priority = 1,enabled = true,description = "TC_002 : Verify departing from value selection")
	public void VerifyDepartingValueSelection()
	{
		try{
			md.login("dasd","dasd");
			Assert.assertEquals(md.departingFromValueSelection(), true);
			
		}catch(Exception e){
			
			System.out.println(e);
			
		}
		
	}
	
	@AfterMethod
	public void apClose(){
		
		md.Appclose();
	}

}
