package com.mercury.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class MethodRepository {
	
	WebDriver driver;
	
	public void browserApplicationLaunch() throws InterruptedException
	{
		//system= class; setProperty= method
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
		
	}
	
	public void login(String username,String pass){
		WebElement uname= driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys(username);
		
		WebElement pswd= driver.findElement(By.xpath("//input[@name='password']"));
		pswd.sendKeys(pass);

        WebElement submit= driver.findElement(By.xpath("//input[@name='login']"));
        submit.click();
		
		//Thread.sleep(5000);
	}
	
	public boolean verifyValidLogin(){
		
		String expTitle = "Find a Flight: Mercury Tours:";
		String actTitle = driver.getTitle();
		
		if (expTitle.equals(actTitle))
		{
			return true;
		}
		else {
			return false;
		}
		
		
	}
	
	public boolean verifyInvalidLogin(){
		
			String expTitle = "Find a Flight: Mercury Tours:";
			String actTitle = driver.getTitle();
		
			if (!expTitle.equals(actTitle))
			{
				return true;
			}
			else {
				return false;
			}
		
		
		}
	
	public boolean VerifyDefaultRoundTrip(){
		
		WebElement radiobuttonOneWay = driver.findElement(By.xpath("//input[@value='oneway']"));
		WebElement radiobuttonRoundTrip = driver.findElement(By.xpath("//input[@value='roundtrip']"));
		
		if(radiobuttonRoundTrip.isSelected()==true && radiobuttonOneWay.isSelected()==false){
			return true;
		}else{
			return false;
		}
		
	}
	
	
	public boolean departingFromValueSelection() throws InterruptedException{
		
		WebElement departValue = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select s1 = new Select(departValue);
		s1.selectByVisibleText("London");
		Thread.sleep(2000);
		if(departValue.getText().equals("London")){
			return true;
		}else{
			return false;
		}
		
	}
	
	
	
	public void Appclose()
	{
		driver.quit();
	}
	
}
